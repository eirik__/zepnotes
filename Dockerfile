FROM apache/zeppelin:0.9.0

USER root
RUN apt-get update
RUN apt-get -y install iputils-ping
RUN apt-get -y install netcat

RUN chmod -R 777 /zeppelin

ENV SPARK_VERSION 2.4.5
ENV HADOOP_PROFILE 2.7
ENV SPARK_HOME /usr/local/spark

# install spark
# RUN curl -s http://www.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop$HADOOP_PROFILE.tgz | tar -xz -C /usr/local/
RUN curl -s http://archive.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop$HADOOP_PROFILE.tgz | tar -xz -C /usr/local/
RUN cd /usr/local && ln -s spark-$SPARK_VERSION-bin-hadoop$HADOOP_PROFILE spark

ADD presto-jdbc-0.216.jar /zeppelin/interpreter/jdbc
ADD presto-jdbc-0.216.jar /zeppelin/interpreter/spark/dep
ADD interpreter.json /zeppelin/conf
ADD zeppelin-env.sh /zeppelin/conf
ADD zeppelin-site.xml /zeppelin/conf
ADD interpreter.sh /zeppelin/bin
ADD fixUID.sh /zeppelin

RUN chmod 777 /zeppelin/bin/interpreter.sh
RUN chmod 777 /zeppelin/conf/interpreter.json
RUN chmod 777 /zeppelin/conf/zeppelin-env.sh
RUN chmod 755 /zeppelin/fixUID.sh

EXPOSE 8080
EXPOSE 4040
EXPOSE 4041
EXPOSE 7078
EXPOSE 7079

RUN chgrp root /etc/passwd && chmod ug+rw /etc/passwd

USER 1000360000

CMD ./fixUID.sh && ./bin/zeppelin.sh
